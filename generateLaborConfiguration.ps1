# checks if paths exist already and if not generates them
function generatePaths($roomName){ 
    If(!(test-path ".$dl$roomName")){ 
        New-Item -Path "." -Name "$roomName" -ItemType "directory"
    }
}

<#
calculates the subnet prefix number. 
in this setup only 2 rooms are supported since an ip address octet can only go up to 255.
3XX would lead to an error in the configuration.
#>
function generatePrefix($roomNumber){
    If($roomNumber -ge 1 -and $roomNumber -lt 3){
        $subPrefix = $room.number * 100 
    }
    Else{
        throw "room number is invalid (only a prefix up to 255 is supported)."
    }
    return $subPrefix
}

# checks if file actualy exists
function checkForFile($file){
    if (!(Test-Path $file)){
        throw "File does not exist: "+ $file
    }
}

# checks for all template files
function checkForTemplates($templateLocation){
    checkForFile $templateLocation"01_SW_Pruefung_config_Teil1.pcc"
    checkForFile $templateLocation"01_SW_Pruefung_config_Teil2.pcc"
    checkForFile $templateLocation"01_FW_Pruefung_config.xml"
    checkForFile $templateLocation"01_FW_Ausgangslage_config.xml"
}

# sets delimiter according to OS
function setDelimiter{
    if ($IsMacOS -or $IsLinux){
        $dl = "/"
    }
    elseif($IsWindows){
        $dl = "\"
    }
    else{
        $dl = "\" # In pwsh 5 $IsWindows is not defined yet and the outdated v5 is still the default in Win10. So I can't throw an Exception here.
        # throw "Only Mac/Windows/Linux are supported"
    }
    return $dl
}

<# 
the generateX functions generate the config files from the templates in $templateLocation
#>
function generateConfFW_Ausgangslage($path, $wan){
    $templFW_Ausgangslage.opnsense.interfaces.wan.ipaddr = $wan
    $templFW_Ausgangslage.save($path)
}

function generateConfFW_Pruefung(
    $path, $wan, $lan, $opt1, $dhcpRange1From, 
    $dhcpRange1To, $dhcpRange2From, $dhcpRange2To, $natDestination
){
    $templFW_Pruefung.opnsense.interfaces.wan.ipaddr = $wan
    $templFW_Pruefung.opnsense.interfaces.lan.ipaddr = $lan
    $templFW_Pruefung.opnsense.interfaces.opt1.ipaddr = $opt1
    $templFW_Pruefung.opnsense.dhcpd.lan.range.from = $dhcpRange1From
    $templFW_Pruefung.opnsense.dhcpd.lan.range.to = $dhcpRange1To
    $templFW_Pruefung.opnsense.dhcpd.opt1.range.from = $dhcpRange2From
    $templFW_Pruefung.opnsense.dhcpd.opt1.range.to = $dhcpRange2To
    $templFW_Pruefung.opnsense.nat.rule.target = $lan
    $templFW_Pruefung.opnsense.nat.rule.destination.address = $natDestination
    $templFW_Pruefung.opnsense.filter.rule[3].destination.address = $lan # destination is only available in the 4th rule.

    $templFW_Pruefung.save($path)
}

function generateConfSW($path, $prefix){
    $templSW_Pruefung1.replace("192.168.1.1","192.168."+$prefix+".1").replace("192.168.1.5","192.168."+$prefix+".5") | Set-Content $path
}

$dl = setDelimiter # gets the path delimiter according to the OS
$templateLocation = "Templates"+$dl # sets the path to where the template files are located

checkForTemplates $templateLocation

$roomList = @() # contains the room objects
$roomList += New-Object PsObject -property @{ name = "LI"; number = 1; numberOfWorkspaces = 20 }
$roomList += New-Object PsObject -property @{ name = "RE"; number = 2; numberOfWorkspaces = 20 }

# get the content of all template files globaly
[xml]$templFW_Ausgangslage = Get-Content ($templateLocation+"01_FW_Ausgangslage_config.xml")
[xml]$templFW_Pruefung = Get-Content ($templateLocation+"01_FW_Pruefung_config.xml")
$templSW_Pruefung1 = Get-Content ($templateLocation+"01_SW_Pruefung_config_Teil1.pcc")
$templSW_Pruefung2 = Get-Content ($templateLocation+"01_SW_Pruefung_config_Teil2.pcc")

# loop through all rooms in $roomList and generate configs accordingly
foreach ($room in $roomList){
    generatePaths $room.name # generate subfolders for all rooms
    $subnetPrefix = generatePrefix $room.number # the prefix number for the third IP address octet x.x.X.x

    # loop through all workspaces and generate the configurations according to the room/workspace number
    for ($i=0; $i -le $room.numberOfWorkspaces; $i++){
        $IP_FW_Wan = "172.17." + ($subnetPrefix + $i) + ".1";
        $IP_FW_Lan = "192.168." + ($subnetPrefix + $i) + ".1";

        generateConfFW_Ausgangslage ("."+$dl+$room.name+$dl+$i+"_Ausgangslage_config.xml") $IP_FW_Wan

        # for a better parameter overview check generateConfFW_Pruefung() 
        generateConfFW_Pruefung ("."+$dl+$room.name+$dl+$i+"_FW_Pruefung_config.xml") $IP_FW_Wan $IP_FW_Lan ("10.10."+($subnetPrefix + $i)+".1") ("192.168."+($subnetPrefix + $i)+".100") ("192.168."+($subnetPrefix + $i)+".199") ("10.10."+($subnetPrefix + $i)+".1") ("10.10."+($subnetPrefix + $i)+".2") ("172.17." + ($subnetPrefix + $i) + ".5")
       
        generateConfSW ($room.name+$dl+$i+"_SW_Pruefung_config_Teil1.pcc") ($subnetPrefix + $i)
        generateConfSW ($room.name+$dl+$i+"_SW_Pruefung_config_Teil2.pcc") ($subnetPrefix + $i)
    }
}